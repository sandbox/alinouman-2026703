<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php
function soul_preprocess_page(&$variables) {
    // Get the entire main menu tree
    $main_menu_tree = menu_tree_all_data('main-menu');
    // Add the rendered output to the $main_menu_expanded variable
   
   $variables['main_menu_expanded'] = menu_tree_output($main_menu_tree);
  
}
function soul_preprocess_html(&$variables) {
drupal_add_css('http://fonts.googleapis.com/css?family=Titillium+Web', array('type' => 'external'));
}
/*
 * Soul Preprocess node
 */
function soul_preprocess_node(&$variables) {
    //dpm($variables['date']);
    $variables['date'] = format_date($variables['node']->created, 'custom', 'm/d/y');
    //dprint_r($variables);
    if($variables['type']=='article'&&$variables['view_mode'] == 'full'){
        $output=soul_prevnext($variables['nid'], $variables['type']);
        $variables['next_article']=$output['next_article'];
        $variables['prev_article']=$output['prev_article'];
        global $base_url;
        $variables['front_page_path']= '<a href="'.$base_url.'">← Back To Articles</a>';
       
    }
}

//function soul_process_node(&$var){
//    dprint_r($var['name']);
//}
/*
 * Soul Prev Next Article
 */

function soul_prevnext($nid, $ntype) {
    $prev = db_query("SELECT nid, title FROM {node} WHERE nid < :nid AND type = :ntype AND status = 1 ORDER BY nid DESC LIMIT    1", array(':nid' => $nid, ':ntype' => $ntype));
    $next = db_query("SELECT nid, title FROM {node} WHERE nid > :nid AND type = :ntype AND status = 1 ORDER BY nid ASC LIMIT 1", array(':nid' => $nid, ':ntype' => $ntype));

    $prev_link = FALSE;
    $next_link = FALSE;

    foreach ($prev as $prev_node) {
        $prev_alias = drupal_lookup_path('alias', 'node' . $prev_node->nid);
        if ($prev_alias) {
            $prev_link = "<a href='" . base_path().$prev_alias . "' title='previous'>Next Article →</a>";
        } else {
            $prev_link = "<a href='" .  base_path().'node/'.$prev_node->nid."' title='previous'>Next Article →</a>";
        }
    }

    foreach ($next as $next_node) {
        $next_alias = drupal_lookup_path('alias', 'node/' . $next_node->nid);
        if ($next_alias) {
            $next_link = "<a href='" . base_path().$next_alias . "' title='next'>← Previous Article</a>";
        } else {
            $next_link = "<a href='" . base_path().'node/'.$next_node->nid . "' title='next'>← Previous Article</a>";
        }
    }

    $output = array('next_article'=>$prev_link,
        'prev_article'=>$next_link
        );

    return $output;
}
?>