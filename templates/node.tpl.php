<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor. node
 */
?>
<?php if($teaser):?>
<?php if(isset($content['field_image'])):?>
<div class="portfolio-wrappper">
    <?php print render($content['field_image']);?>
    <h1><?php print $title; ?></h1>
</div>
<?php endif;?>
<?php endif; ?>
<?php if(!$teaser):?>
<article>
    <h1><?php print $title;?></h1>
    <div id="home-link"><?php
    if (isset($front_page_path))
    print $front_page_path;
    
    ?></div>
    <?php 
    hide($content['comments']);
    print render($content);
    //dprint_r($content);
           
    ?>
    <?php if($node->type=='article'):?>
    <ul><li><em>Date:</em><span><?php print $date;?></span></li>
        <li><em>Author:</em><span><?php print $name ?></span></li>
    </ul>
    <footer>
        <nav id="nav-article">
        <?php if(isset($next_article)):?>
            <span class='right'>
        <?php print $next_article;?>
            </span>
        <?php endif; ?>
         <?php if (isset($prev_article)): ?>
            <span class='left'>
             <?php print $prev_article; ?>
            </span>
         <?php endif; ?>
        </nav>
    </footer>
    <?php endif;?>
</article>
<?php endif; ?>



