<?php
/*
 * Page.tpl.php for Soul
 */
?>
<div id="page-wrapper">

    <header>
        <?php if ($logo): ?>
            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
                <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
            </a>
        <?php endif; ?>
        <?php if ($site_name || $site_slogan): ?>
            <?php if ($site_name): ?>
                <div id="site-name"><?php print $site_name; ?></div>
            <?php endif; ?>
            <?php if ($site_slogan): ?>
                <div id="site-slogan"><?php print $site_slogan; ?></div>
            <?php endif; ?>

        <?php endif; ?>
        <?php print render($page['header']); ?>
        <?php if ($main_menu): ?>
                    <div id="main-menu" class="navigation">
                        <?php print render($main_menu_expanded); ?>
                    </div> <!-- /#main-menu -->
          <?php endif; ?>     
    </header>
    <section id="content">
    <?php if ($messages): ?>
            <div id="messages">
                    <?php print $messages; ?>
             </div> <!-- /.section, /#messages -->
     <?php endif; ?>
             <?php if ($tabs): ?>
                 <div class="tabs">
                     <?php print render($tabs); ?>
                 </div>
             <?php endif; ?>
             <?php if ($action_links): ?>
                 <ul class="action-links">
                     <?php print render($action_links); ?>
                 </ul>
             <?php endif; ?>
             <?php print render($page['content']); ?>
             <?php print $feed_icons; ?>
    </section>
</div>